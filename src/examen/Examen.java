/**
 * Programa con Treemap que inserta cinco productos,les asigna una clave,que será su
 * nombre y tambien un precio.
 * Despues se visualiza todo ordenandolo alfabeticamente segun su clave.
 * 
 */
//Pequeño cambio para el 2 commit
package examen;
import java.util.*;
/**
 *
 * @author rguidoarias
 * Está clase contiene los atributos nombre y precio 
 * comunes a producto.
 */
public class Examen {
    /**
     * @param nombre 
     */
private String nombre;
private float precio;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /**
         * @param p 
         * Aqui se guarda el TreeMap 
         * y se le da el valor Clave,que será su nombre y el precio tipo float.
         */
       
        TreeMap p = new TreeMap();
        p.put("Tomate",new Float(1.5));
        p.put("Pan",new Float(1));
        p.put("Queso",new Float(2.25));
        p.put("Aceite",new Float(4.55));
        p.put("Kiwi",new Float(3));
        Set set = p.entrySet();
        Iterator i = set.iterator();
        /**
         * Bucle que mostrará los datos recibidos y los ordenará
         * alfabéticamente por su clave.
         */
        while(i.hasNext()){
            Map.Entry ob = (Map.Entry)i.next();
            System.out.print(ob.getKey()+" :");
            System.out.println(ob.getValue());
        }
        
    }

    /**
     * Regresa el nombre del producto
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Regresa el precio del producto
     * @return the precio
     */
    public float getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
  
}
